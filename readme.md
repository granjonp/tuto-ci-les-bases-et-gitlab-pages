# Exemple d'utilisation de la CI

[[_TOC_]]

Un exemple pour ...

* bien comprendre les principes de l'intégration continue
* maitriser les outils (gitlab-ci) et être capable de les mettre en place dans un projet gitlab.

Démo/training en lien avec le cours [Gestion de projets - Outils collaboratifs - Gitlab et git](https://pole-calcul-formation.gricad-pages.univ-grenoble-alpes.fr/outils-collaboratifs-gitlab/cours/)


Les modèles de fichiers yml à utiliser au cours de la démo sont disponibles dans le répertoire [templates](./templates).

<p xmlns:cc="http://creativecommons.org/ns#" >This work is licensed under <a href="http://creativecommons.org/licenses/by-nc-sa/4.0/?ref=chooser-v1" target="_blank" rel="license noopener noreferrer" style="display:inline-block;">CC BY-NC-SA 4.0<img style="height:12px!important;margin-left:3px;vertical-align:text-bottom;" src="https://mirrors.creativecommons.org/presskit/icons/cc.svg?ref=chooser-v1"><img style="height:22px!important;margin-left:3px;vertical-align:text-bottom;" src="https://mirrors.creativecommons.org/presskit/icons/by.svg?ref=chooser-v1"><img style="height:12px!important;margin-left:3px;vertical-align:text-bottom;" src="https://mirrors.creativecommons.org/presskit/icons/nc.svg?ref=chooser-v1"> <img style="height:12px!important;margin-left:3px;vertical-align:text-bottom;" src="https://mirrors.creativecommons.org/presskit/icons/sa.svg?ref=chooser-v1"></a></p>

## Premier exemple : construction et mise à disposition d'un pdf

### Objectifs

- produire automatiquement un pdf et le mettre en ligne à partir d'un .tex
- le rendre disponible sur la page du projet Gitlab

Point de départ : 
- ce projet gitlab
- un fichier [article.tex](./article.tex)
- un fichier [gitlab-ci.yml](.gitlab-ci.yml)

contenant les lignes suivantes :

```yaml
make_pdf:
  stage: build
  image: aergus/latex:latest
  script:
     - pdflatex article.tex
     - mv article.pdf docs
  artifacts:
    paths:
      - docs/article.pdf
    expire_in: 1 week
```

Fichier yml :

* langage yaml
* une série de mots-clés et d'attributs définissant les caractéristiques du job. Doc : [gitlab-ci-keywords](https://docs.gitlab.com/ee/ci/yaml/)

Remarque : les differentes versions du .yml sont dans le répertoire [modeles](./modeles)

:bulb: *si vous ajoutez ce fichier via l'interface gitlab (ou le WebIDE) vous pourrez profiter des templates de fichier .gitlab-ci.yml*  

La présence de ce fichier dans le repository va entrainer :

* l'activation de l'intégration continue
* la création et l'exécution d'un **job** nommé make_pdf

    * mot-clé **script** : les commandes à exécuter
    * mot-clé **image** : permet de définir l'environnement où seront exécutées les commandes
    * mot-clé **artifacts** : ce qui doit être conservé à la fin

### Vocabulaire

* **job** : une suite d’actions exécutées sur un runner.
  Chaque job est indépendant. Rien n’est conservé à la fin du job (sauf demande explicite via les artifacts).

* **runner** : une machine hôte qui va exécuter Docker et faire tourner notre job.

* **Pipeline** : une série de jobs exécutés sur un ou plusieurs runners.

  Chaque pipeline correspond à un commit et vous retrouvez dans le pipeline les jobs définis dans votre script (make_pdf dans votre cas).

* **stage** : un “étage” du pipeline, pouvant contenir plusieurs job exécutés en parallèle.
  
  * Pour passer au stage suivant, tous les jobs doivent avoir réussi (sauf en cas d'utilisation du mot-clé **needs**).
  * Chaque job doit faire partie d'un stage.
  * Pour plus de détails, voir [gitlab-ci stages](https://docs.gitlab.com/ee/ci/yaml/#stage)

* **artifacts** : des répertoires ou des fichiers à conserver et à transmettre d’un job à l’autre.

:warning: ces artifacts sont conservés et stockés sur la plateforme et, selon les projets, peuvent rapidement occuper beaucoup d'espace.

**expire_in** : fixe une durée de vie et assure la destruction des artifacts au bout d'une semaine. Cela permet d'éviter d'encombrer le serveur inutilement.
Pour plus de détails à ce sujet, voir [Defining job artifacts](https://docs.gitlab.com/ee/ci/pipelines/job_artifacts.html).


### Résultats

Pour vérifier l'impact de l'ajout du fichier .gitlab-ci.yml, visitez le menu CI/CD->pipelines du projet.

Dans le menu CI/CD->pipelines, si vous cliquez sur un pipeline puis sur un job, vous obtiendrez un accès direct à la console. Par exemple :

![gitlab-ci-jobs.jpeg](images/gitlab-ci-jobs.jpeg)

Lorsque vous êtes sur la page du job, vous pouvez également naviguer dans les artifacts (menu à droite) et avoir accès aux fichiers générés et conservés, article.pdf dans notre exemple.

### Quelques commentaires

* Pour chaque job, "clonage" automatique du projet sur l'hôte

  Inutile de préciser
  ```
  git clone votreprojet
  ```
  après le mot-clé script : le projet qui contient le fichier .gitlab-ci.yml est automatiquement cloné sur la nouvelle machine. 
  Vos commandes seront exécutées dans le répertoire cloné.

* Durant le job, le répertoire du projet est identifié par la variable CI_PROJECT_DIR.

A noter que de nombreuses variables d'environnement sont définies par défaut par gitlab-ci et utilisables dans vos scripts, voir [GitLab CI/CD environment variables](https://docs.gitlab.com/ee/ci/variables/).

:bulb: exécutez la commande **env** dans votre script pour afficher toutes les variables d'environnement :
```yml
  script:
     - env
     - pdflatex article.tex
```

* Attention à la syntaxe, le yaml peut-être pénible ...

Nous vous conseillons d'utiliser le menu **Build Pipeline Editor** pour éditer votre fichier .gitlab-ci.yml.  
Des templates, un outil de validation/vérification de la syntaxe yaml ... Faites le test !

**Avant d'aller plus loin**

- Assurez vous d'avoir bien fait fonctionner la CI dans votre projet
- trouvez le pdf dans les artifacts
- naviguez dans les différents menus et n'hésitez pas à poser des questions : gitlab-ci est un outil très complet et nous ne pourrons pas tout aborder !

## Mise en ligne et création d'une page web : **gitlab-pages**

### Principe et mise en place

Un outil permettant de publier un site web statique associé à votre projet : [gitlab-pages](https://about.gitlab.com/features/pages/).

1. Choisisez un générateur de site (Pelican, Sphinx, Mkdocs ..., voir [exemples ici](https://about.gitlab.com/blog/2016/06/17/ssg-overview-gitlab-pages-part-3-examples-ci/#environment-python). Un outil pour générer des pages html

Pour notre exemple, nous avons choisis [mkdocs](https://www.mkdocs.org/)

2. Ecrivez vos pages dans le langage choisi (markdown, rst ...) puis enregistrez les fichiers dans votre projet Gitlab, avec le fichier de config de votre
générateur.

Voir pour notre exemple [docs/index.md](./docs/index.md) et le fichier [mkdocs.yml](./mkdocs.yml)

3. Un outil de votre choix (sphinx, mkdocs ...), dépendant du générateur choisi, sera appelé dans un job d'**intégration continue** pour générer des **pages web statiques** (html et autres fichiers). Notez que cette étape est optionnelle : vous pouvez sauvegarder les fichiers html directement dans votre projet git.

Dans notre exemple, la ligne pour construire les pages html (dans le répertoire site) sera :

```
mkdocs build
```

4. Un job d'intégration continue spécifique qui DOIT s'appeler *pages* est chargé de publier vos pages sur un serveur qui sera nommé
 <NOM_DE_VOTRE_GROUPE>.gricad-pages.univ-grenoble-alpes.fr/<NOM_SOUS_GROUPE>/<NOM_PROJET>


Voici quelques exemples de pages publiées via gitlab-pages :

  * [Les pages web de nos formations](https://pole-calcul-formation.gricad-pages.univ-grenoble-alpes.fr/ced)
  * [Les pages de doc d'un code de calcul](https://nonsmooth.gricad-pages.univ-grenoble-alpes.fr/siconos/)


Nous allons compléter notre projet (celui avec article.tex) pour créer une page web contenant entre autre un lien vers le pdf généré par le job make_pdf.

Les sources (markdown) pour générer la page sont dans le répertoire docs. Nous utilisons mkdocs, un outil qui permet de créer du contenu web
à partir de markdown.

* ajoutez au fichier .gitlab-ci.yml les lignes suivantes :

```yaml
pages:
  stage: test
  image: python:3.8.5-slim-buster
  script:
  - python -m pip install --upgrade pip mkdocs-bootswatch
  - mkdocs build
  - mv site public
  artifacts:
    paths:
    - public
```

Comme pour le job make_pdf, on retrouve :
* un stage (test)
* une image Docker sur laquelle le job sera exécuté, ici python:3.8.5-slim-buster
* une série de commandes après le mot-clé **script**
* des artifacts : ici nous souhaitons conserver le repertoire 'public' qui contient tous les fichiers html générés par pandoc.

### Quelques commentaires

* Pour créer une page web, le job doit s'appeler pages
* Un job 'deploy' sera automatiquement créé et associé à pages. Son rôle est de 'déployer' les pages web à un endroit accessible

:point_right: vérifiez la présence de ce job dans le pipeline

* L'adresse du site web créé sera toujours de la forme NOM_GROUPE.gricad-pages.univ-grenoble-alpes.fr/NOM_PROJET.

  :bulb: Retrouvez l'adresse de votre page dans le menu **Deploy->Pages**

* Vous pouvez utiliser n'importe quel outil pour créer votre site web (hugo, mkdocs, sphinx ...). Le point important est que tous les fichiers générés doivent se trouver dans le répertoire 'public' passé dans les artifacts.


### Résultats

Si tout fonctionne correctement, vous aurez accès à votre site web via le lien suivant :
https://pole-calcul-formation.gricad-pages.univ-grenoble-alpes.fr/outils-collaboratifs-gitlab/demos/ci-example-basics

La page web créée peut-être publique ou privée (i.e. accessible uniquement aux membres du projet après authentification). 

:bulb: Le contrôle de cet accès se fait via le menu "Settings, General, Features etc, Pages. 

:point_right:  Faites le test : changez les droits de vos pages, allez visiter le site de votre voisin.

## Compléments

Sur de gros projets, l'intégration continue peut rapidement devenir consommatrice de ressoures :frowning:

De plus il n'est pas toujours judicieux ou utile de lancer systématiquement ou entièrement la CI après chaque modification du code source.
On peut par exemple souhaiter modifier la doc sans lancer la recompilation du code, éditer des fichiers qui n'auront pas d'impact sur les pages web etc

Il existe différentes manières de contrôler la CI, entre autres :

* via le contenu des messages de commit.
  Par exemple, si le message commence par [skip CI], les jobs ne sont pas lancés. 
 
* en précisant explicitement des règles de fonctionnement dans le fichier yml.

Par exemple, en ajoutant au début du fichier .gitlab-ci.yml les lignes suivantes :

```yaml
workflow:
  rules:
    - if: $CI_COMMIT_MESSAGE =~ /\[publish\].*/i
      when: always
    - when: never
```

Dans ce cas, si le message de commit contient [publish],  la règle est de lancer la CI. Dans le cas contraire, rien ne se passe.

Ce genre de règles peut également être décrite directement dans un job.

:warning: fixer des règles est une bonne habitude à prendre pour éviter de lancer tout le process au moindre changement dans le code.

Rappelez vous de [Je code: les bonnes pratiques en écoconception](https://scalde.gricad-pages.univ-grenoble-alpes.fr/comm-et-formations/2021_02_11-atelier-outils-collaboratifs/pdf/Plaquette_Ecoinfo_EcoConception_Logicielle_PDF_v1.0.pdf) !


